from debug import *


def test_aggregate():
	test_aggregate_tuple()
	test_aggregate_record()
	test_aggregate_variant()

def test_aggregate_tuple():
	print('\n\n***************\n*    Tuple    *\n***************')

	tup = Tuple_expr([expr(0), expr(L_AND_expr(True, False)), expr(2), expr(True)])
	debug(tup)

	e = Index_proj_expr(tup, 1)
	debug(e)

def test_aggregate_record():
	print('\n\n****************\n*    Record    *\n****************')

	rec = Record_expr([0,   10,  20,  Multiplication_expr(4, 2), 40,  True],
	                  ["a", "b", "c", "d",                       "e", "f" ])
	debug(rec)

	e = Access_proj_expr(rec, "d")
	debug(e)

def test_aggregate_variant():
	print('\n\n*****************\n*    Variant    *\n*****************')

	vari = Variant_expr(50, ["a", "b", "c", "d", "e", "f"], "d")
	debug(vari)

	e = Access_proj_expr(vari, "d")
	debug(e)
