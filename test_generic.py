from debug import *


def test_generic():
	test_generic_type()
	test_generic_lambda()
	test_instantiation()
	test_template_lambda_call()

def test_generic_type():
	t1 = Universal_type(["X", "Y"], Function_type(["X"], "X"))
	resolve(t1)
	print(t1)

def test_generic_lambda():
	less = \
	Lambda_expr(
		[("a", "T"), ("b", "U")],
		LT_expr("a", "b"))
	g = Generic_expr(["T", "U"], less)
	debug(g)

def test_instantiation():
	less = \
	Lambda_expr(
		[("a", "T"), ("b", "U")],
		LT_expr("a", "b"))
	g = Generic_expr(["T", "U"], less)
	i = Instantiate_expr(g, [int, int])
	debug(i)

def test_template_lambda_call():
	less = \
	Lambda_expr(
		[("a", "T"), ("b", "U")],
		LT_expr("a", "b"))
	g = Generic_expr(["T", "U"], less)
	i = Instantiate_expr(g, [int, int])
	call = Call_expr(i, [42, 88])
	debug(call)
