from lang import *


def is_reducible(e):
	assert isinstance(e, Expr)
	return not is_value(e)

def reduce(e):
	assert isinstance(e, Expr)
	print(e)
	while is_reducible(e):
		e = step(e)
		print(e)
	return e

def step(e):
	assert isinstance(e, Expr)
	assert is_reducible(e)

	if isinstance(e, Unary_expr):
		return step_Unary(e)
	if isinstance(e, Binary_expr):
		return step_Binary(e)
	if isinstance(e, Ternary_expr):
		return step_Ternary(e)
	if isinstance(e, Multiary_expr):
		return step_Multiary(e)

	print(f'Assert: {type(e)}')
	assert False

def step_Unary(e):
	assert isinstance(e, Unary_expr)

	if isinstance(e, Variant_expr):
		return step_variant(e)
	if isinstance(e, Projection_unary_expr):
		return step_projection(e)

	#################################
	#           Judgement           #
	#################################
	#                               #
	# ----------------------------- #
	# op v1 -> [op `v1`]            #
	#################################
	#    e1 -> e1'                  #
	# ----------------------------- #
	# op e1 -> op e1'               #
	#################################
	if is_value(e.expr):
		if type(e) is Abstraction_expr:
			logger('***  step_Unary->Abstraction_expr  ***')

		op = type(e).get_operation()
		return expr(op(e.expr.value))

	ExprNode = type(e)
	return ExprNode(step(e.expr), e.proj)

def step_variant(e):
	assert type(e) is Variant_expr

	ExprNode = type(e)
	x = ExprNode(step(e.expr), e.fields, e.active)
	x.type = e.type
	return x

def step_projection(e):
	assert isinstance(e, Projection_unary_expr)
	# assert False
	return e.expr[e.proj]

def step_Binary(e):
	assert isinstance(e, Binary_expr)

	#################################
	#           Judgement           #
	#################################
	#                               #
	# ----------------------------- #
	# v1 op v2 -> [`v1` op `v2`]    #
	#################################
	#       e1 -> e1'               #
	# ----------------------------- #
	# e1 op e2 -> e1' op e2         #
	#################################
	#       e2 -> e2'               #
	# ----------------------------- #
	# v1 op e2 -> v1 op e2'         #
	#################################
	if is_value(e.lhs) and is_value(e.rhs):
		if type(e) is Application_expr:
			return step_Application(e)

		op = type(e).get_operation()
		return expr(op(e.lhs.value, e.rhs.value))

	ExprNode = type(e)
	if is_reducible(e.lhs):
		return ExprNode(step(e.lhs), e.rhs)
	if is_reducible(e.rhs):
		return ExprNode(e.lhs, step(e.rhs))
	assert False

def step_Ternary(e):
	assert isinstance(e, Ternary_expr)

	###########################################
	#                Judgement                #
	###########################################
	#                                         #
	# --------------------------------------- #
	# v1 op e2 : e3 -> [`v1` op `e2` : `e3`]  #
	###########################################
	#            e1 -> e1'                    #
	# --------------------------------------- #
	# e1 op e2 : e3 -> e1' op e2 : e3         #
	###########################################

	if is_value(e.e1):
		op = type(e).get_operation()
		return expr(op(e.e1.value, e.e2, e.e3))

	ExprNode = type(e)
	if is_reducible(e.e1):
		return ExprNode(step(e.e1), e.e2, e.e3)

	assert False

def step_Application(e):
	assert type(e) is Application_expr

	#################################
	#           Judgement           #
	#################################
	#    e1 -> e1'                  #
	# ----------------------------- #
	# e1 e2 -> e1' e2               #
	#################################
	#       e2 -> e2'               #
	# ----------------------------- #
	# λx.e1 e2 -> λx.e1 e2'         #
	#################################
	#                               #
	# ----------------------------- #
	# λx.e1 v -> [x -> v] e1        #
	#################################
	if type(e.lhs) is not Abstraction_expr:
		raise Exception(f'Application of non-lambda. abstraction: "{e.lhs}", argument: "{e.rhs}"')

	substitutions = { e.lhs.var: e.rhs }
	return substitute(e.lhs.expr, substitutions)

def step_Multiary(e):
	if type(e) is Call_expr:
		return step_Call(e)

	if is_reducible(e):
		ExprNode = type(e)
		for i in range(len(e.exprs)):
			if is_reducible(e.exprs[i]):
				if ExprNode is Record_expr:
					return ExprNode(e.exprs[:i] + [step(e.exprs[i])] + e.exprs[i+1:], e.fields)
				return ExprNode(e.exprs[:i] + [step(e.exprs[i])] + e.exprs[i+1:])

	assert False

def step_Call(e):
	assert type(e) is Call_expr
	# Call a lambda function with arguments.
	###############################################
	#                  Judgement                  #
	###############################################
	#                  e0 ~> e0'                  #
	# ------------------------------------------- # Call-0
	# e0(e1, e2, ..., en) ~> e0'(e1, e2, ..., en) #
	###############################################
	#                  ei ~> ei'                  #
	# ------------------------------------------- # Call-i
	# (λ(x1, x2, ..., xn).e0)(..., ei, ...) ~>    # Applies for each argument from 1 to n.
	#     (λ(x1, x2, ..., xn).e0)(..., ei', ...)  #
	###############################################
	#                  ei ~> ei'                  #
	# ------------------------------------------- # Call-n
	# (λ(x1, x2, ..., xn).e0)(e1, e2, ..., en) ~> #
	#     [x1->e1, x2->e2, ..., xn->en]e1         #
	###############################################

	if is_reducible(e.func):
		return Call_expr(step(e.func), e.args)

	if len(e.args) < len(e.func.vars):
		raise Exception("too few arguments")
	if len(e.args) > len(e.func.vars):
		raise Exception("too many arguments")

	# Step next reducible argument
	for i in range(len(e.args)):
		if is_reducible(e.args[i]):
			return Call_expr(e.func, e.args[:i] + [step(e.args[i])] + e.args[i+1:])

	# Map parameters to arguments.
	substitutions = {}
	for i in range(len(e.args)):
		substitutions[e.func.vars[i]] = e.args[i]

	# Substitute through the definition.
	return substitute(e.func.expr, substitutions);
