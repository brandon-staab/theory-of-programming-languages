from AST import *


def is_value(e):
	assert isinstance(e, Expr)

	if type(e) is Variant_expr:
		return is_value(e.expr)
	elif isinstance(e, Aggregate_multary_expr):
		for expr in e.exprs:
			if not is_value(expr):
				return False
		return True

	return isinstance(e, (Nullary_expr, LambdaAbstraction_unary_expr))

def size(e):
	assert isinstance(e, Expr)

	if is_value(e):
		return 1
	if isinstance(e, Unary_expr):
		return 1 + size(e.expr)
	if isinstance(e, Binary_expr):
		return 1 + size(e.lhs) + size(e.rhs)
	if isinstance(e, Ternary_expr):
		return 1 + size(e.e1) + size(e.e2) + size(e.e3)
	if isinstance(e, Multiary_expr):
		if type(e) is Call_expr:
			return 1 + sum(map(size, e.args))
		if isinstance(e, Aggregate_multary_expr):
			return 1 + sum(map(size, e.exprs))
	assert False

def height(e):
	assert isinstance(e, Expr)

	if is_value(e):
		return 0
	if isinstance(e, Unary_expr):
		return 1 + height(e.expr)
	if isinstance(e, Binary_expr):
		return 1 + max(height(e.lhs), height(e.rhs))
	if isinstance(e, Ternary_expr):
		return 1 + max(height(e.e1), height(e.e2), height(e.e3))
	if isinstance(e, Multiary_expr):
		if type(e) is Call_expr:
			return 1 + max(map(size, e.args))
		if isinstance(e, Aggregate_multary_expr):
			return 1 + max(map(size, e.exprs))
	assert False

def is_same(x, y):
	return is_same_ast(expr(x), expr(y))

def is_same_ast(e1, e2):
	assert isinstance(e1, Expr)
	assert isinstance(e2, Expr)

	if type(e1) is not type(e2):
		return False

	if is_value(e1):
		return e1 == e2
	if isinstance(e1, Projection_unary_expr):
		if e1.proj != e2.proj:
			return False
		return is_same_ast(e1.expr, e2.expr)

	if isinstance(e1, Unary_expr):
		return is_same_ast(e1.expr, e2.expr)
	if isinstance(e1, Binary_expr):
		return is_same_ast(e1.lhs, e2.lhs) and is_same_ast(e1.rhs, e2.rhs)
	if isinstance(e1, Ternary_expr):
		return is_same_ast(e1.e1, e2.e1) and is_same_ast(e1.e2, e2.e2) and is_same_ast(e1.e3, e2.e3)
	if isinstance(e, Multiary_expr):
		if type(e) is Call_expr:
			if not is_same_ast(e1.fn, e2.fn):
				return False
			if len(e1.args) != len(e2.args):
				return False
			for i in range(len(e1.args)):
				if not is_same_ast(e1.args[i], e1.args[i]):
					return False
			return True
		if isinstance(e, Aggregate_multary_expr):
			for i in range(len(e1.exprs)):
				if not is_same_ast(e1.exprs[i], e2.exprs[i]):
					return False
			return True

	assert False

from substitute import substitute
from resolution import resolve

from instantiate import instantiate
from type_checking import get_expr_type

from small_step import reduce
from big_step import evaluate
