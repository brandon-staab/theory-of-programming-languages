from lang import *


def substitute(x, sub):
	logger(f"x = {repr(x)},  sub = {repr(sub)}")

	assert isinstance(x, LangAtom)
	assert type(sub) is dict
	for key, value in sub.items():
		assert isinstance(key, LangAtom)
		assert isinstance(value, LangAtom)

	if isinstance(x, Expr):
		return substitute_expr(x, sub)
	elif isinstance(x, Type):
		return substitute_type(x, sub)
	else:
		logger(f"Assert: {repr(x)}")
		assert False

def substitute_expr(e, sub):
	assert isinstance(e, Expr)
	assert type(sub) is dict
	for V, E in sub.items():
		assert isinstance(V, Decl)
		assert isinstance(E, (Expr, Type))

	if isinstance(e, Nullary_expr):
		return substitute_nullary_expr(e, sub)
	elif isinstance(e, Unary_expr):
		return substitute_unary_expr(e, sub)
	elif isinstance(e, Binary_expr):
		return substitute_binary_expr(e, sub)
	elif isinstance(e, Ternary_expr):
		return substitute_ternary_expr(e, sub)
	elif isinstance(e, Multiary_expr):
		return substitute_multiary_expr(e, sub)
	else:
		logger(f"Assert: {repr(e)}")
		assert False

def substitute_nullary_expr(e, sub):
	assert isinstance(e, Nullary_expr)
	assert type(sub) is dict

	if type(e) is Id_expr:
		# [x->s]x = v
		# [x->s]y = y (y != x)
		if e.ref in sub:
			return sub[e.ref]
		else:
			return e
	else:
		return e

def substitute_unary_expr(e, sub):
	assert isinstance(e, Unary_expr)
	assert type(sub) is dict

	if type(e) is Variant_expr:
		assert False
	if isinstance(e, Projection_unary_expr):
		expr = substitute_expr(e.expr, sub)
		Node = type(e)
		return Node(expr, e.proj)
	elif type(e) is Lambda_expr:
		parms = [Var_decl(parm.id, substitute_type(parm.type, sub)) for parm in e.vars]
		expr = substitute_expr(e.expr, sub)
		return Lambda_expr(parms, expr)

	# [x->s]@e1 = @[x->s]e1
	expr = substitute(e.expr, sub)
	Node = type(e)

	return Node(expr)

def substitute_binary_expr(e, sub):
	assert isinstance(e, Binary_expr)
	assert type(sub) is dict

	# [x->s](e1 @ e2) = [x->s]e1 @ [x->s]e2
	lhs = substitute(e.lhs, sub)
	rhs = substitute(e.rhs, sub)
	Node = type(e)

	return Node(lhs, rhs)

def substitute_ternary_expr(e, sub):
	assert isinstance(e, Ternary_expr)
	assert type(sub) is dict

	# [x->s](e1 @ e2 : e3) = [x->s]e1 @ [x->s]e2 : [x->s]e3
	e1 = substitute(e.e1, sub)
	e2 = substitute(e.e2, sub)
	e3 = substitute(e.e3, sub)
	Node = type(e)

	return Node(e1, e2, e3)

def substitute_multiary_expr(e, sub):
	assert isinstance(e, Multiary_expr)

	if type(e) is Call_expr:
		assert False
	elif type(e) is Tuple_expr:
		assert False
	elif type(e) is Record_expr:
		assert False
	else:
		assert False

def substitute_type(t, sub):
	assert isinstance(t, Type)
	assert type(sub) is dict
	for V, T in sub.items():
		assert type(V) is Type_decl
		assert isinstance(T, Type)

	if isinstance(t, Nullary_type):
		return substitute_nullary_type(t, sub)
	elif isinstance(t, Binary_type):
		return substitute_binary_type(t, sub)
	elif isinstance(t, Multiary_type):
		return substitute_multiary_type(t, sub)
	else:
		logger(f"Assert: {repr(t)}")
		assert False


def substitute_nullary_type(t, sub):
	assert isinstance(t, Nullary_type)

	if type(t) is Id_type:
		# [X->s]X = s
		# [X->s]Y = Y (Y != X)
		if t.ref in sub:
			return sub[t.ref]
		else:
			return t
	else:
		return t

def substitute_binary_type(t, sub):
	assert isinstance(t, Binary_type)
	assert False

def substitute_multiary_type(t, sub):
	assert isinstance(t, Multiary_type)

	if type(t) is Function_type:
		parms = [substitute_type(parm, sub) for parm in t.parms]
		ret = substitute_type(t.ret, sub)
		return Function_type(parms, ret)
	else:
		assert False
