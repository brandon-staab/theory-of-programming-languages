from debug import *

def test_lambda():
	print('\n\n****************\n*    Lambda    *\n****************')
	# # True is λa.λb.a
	# t = Abstraction_expr(
	# 		Var_decl('a', None),
	# 		Abstraction_expr(Var_decl('b', None), expr('a')))
	# print(f'True = {t}')
	#
	# # False is λa.λb.b
	# f = Abstraction_expr(
	# 		Var_decl('a', None),
	# 		Abstraction_expr(Var_decl('b', None), expr('b')))
	# print(f'False = {f}')
	#
	# # and is λp.λq.((p q) p)
	# l_and = \
	# 	Abstraction_expr(
	# 		Var_decl('p', bool_type),
	# 		Abstraction_expr(
	# 			Var_decl('q', bool_type),
	# 			Application_expr(
	# 				Application_expr(
	# 					expr('p'),
	# 					expr('q')),
	# 				expr('p'))))
	# print(f'and = {l_and}\n')
	#
	# # ((λp.λq.((p q) p) λa.λb.a) λa.λb.b) -> λa.λb.b
	# lamb = Application_expr(Application_expr(l_and, t), f)
	# debug(lamb)

	# λside:int.(side * side)
	square = \
		Abstraction_expr(
			Var_decl('side', int_type),
			Multiplication_expr(
				expr('side'),
				expr('side')))


	# (λside.(side * side) 4)
	call = Application_expr(square, Integer_lit_expr(4))
	debug(call)
