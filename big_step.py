from lang import *


import copy
clone = copy.deepcopy

#### Closure ##################################################

class Closure:
	def __init__(self, abs, env):
		assert isinstance(abs, LambdaAbstraction_unary_expr)
		assert type(env) is dict

		self.abs = abs
		self.env = clone(env)

	def __str__(self):
		env = ", ".join(map(str, self.env))
		return f'<closure|abs:{self.abs}|env:[{env}]>'

	def __repr__(self):
		env = ", ".join(map(repr, self.env))
		return f'<closure|{repr(self.abs)}|env:[{env}]|{address(self)}>'

#### Evaluate #################################################

def evaluate(e, store = {}):
	assert isinstance(e, Expr)
	assert type(store) is dict

	if isinstance(e, Literal_nullary_expr):
		return e.value

	if isinstance(e, Aggregate_multary_expr):
		return eval_aggregate(e, store)

	special_table = {
		Id_expr: eval_id,
		Variant_expr: eval_variant,
		Abstraction_expr: eval_abstraction,
		Lambda_expr: eval_lambda,
		Application_expr: eval_application,
		Call_expr: eval_call,
		Index_proj_expr: eval_index_proj,
		Access_proj_expr: eval_access_proj
	}

	if type(e) in special_table:
		return special_table[type(e)](e, store)

	op = type(e).get_operation()
	if isinstance(e, Unary_expr):
		return op(evaluate(e.expr, store))
	if isinstance(e, Binary_expr):
		return op(evaluate(e.lhs, store), evaluate(e.rhs, store))
	if isinstance(e, Ternary_expr):
		x = op(evaluate(e.e1, store), e.e2, e.e3)
		return evaluate(x, store)
	assert False

def eval_id(e, store):
	assert type(e) is Id_expr
	assert type(store) is dict
	# Evaluate an id-expression by finding it's stored value.
	#
	# ------------- E-Id
	# S ⊢ x ⇓ S[x]

	# logger('eval_id(e, store)')
	# logger(f'e = {repr(e)}')
	# logger(f'e.ref = {repr(e.ref)}')
	# logger(f'store = [{", ".join(map(repr, store))}]')
	# logger(f"store[e.ref] = {store[e.ref]}")

	if e.ref not in store:
		raise Exception(f"{e.ref} is not in the store")

	return store[e.ref]

def eval_variant(e, store):
	assert type(e) is Variant_expr

	ExprNode = type(e)
	x = ExprNode(evaluate(e.expr, store), e.fields, e.active)
	x.type = e.type
	return x

def eval_abstraction(e, store):
	assert type(e) is Abstraction_expr
	assert type(store) is dict
	# The evaluation of an abstraction produces a closure.
	#
	# --------------------- E-Abs
	# S ⊢ λx.e ⇓ <λx.e, S>
	#
	# Note that the store is cloned; it is not a reference to the current
	# store (that would cause real problems). We could also compute the
	# minimal store by finding all free variables in e and then building
	# a single mapping of those values.
	return Closure(e, store)

def eval_lambda(e, store):
	assert type(e) is Lambda_expr
	assert type(store) is dict
	# The evaluation of a lambda abstraction produces a closure.
	#
	# ----------------------------------------------------- E-Lambda
	# S ⊢ λ(x1, x2, ..., xn).e ⇓ <λ(x1, x2, ..., xn).e, S>
	return Closure(e, store)

def eval_application(e, store):
	assert type(e) is Application_expr
	assert type(store) is dict
	# Evaluate an application.
	#
	# S ⊢ e1 ⇓ <λx.e3, S'>   S ⊢ e2 ⇓ v1   S', x=v1 ⊢ e3 ⇓ v2
	# ---------------------------------------------------------- E-App
	#                     S ⊢ e1 e2 ⇓ v2
	#
	# In other words, evaluate the LHS and RHS. Use the environment
	# of the closure (S'), along with the new variable binding to
	# evaluate the closure's body.
	#
	# Note that this does not handle recursion correctly because the
	# store flat.
	c = evaluate(e.lhs, store)
	# logger(f"c = {c}")

	if type(c) is not Closure:
		raise Exception("cannot apply a non-closure to an argument")

	# Evaluate argument
	v = evaluate(e.rhs, store)
	# logger(f"v = {v}")
	# assert type(v) is not Closure
	if type(v) is Closure:
		raise Exception("Fix recursion.")
	assert isinstance(v, Expr)

	# Build the new environment
	env = clone(c.env)
	env[c.abs.var] = v

	# logger('eval_application(e, store)')
	# logger(f'e = {repr(e)}')
	# logger(f'store = {repr(store)}')
	#
	# logger(f'c = {repr(c)}')
	# logger(f'a.abs.var = {repr(c.abs.var)}')
	# logger(f'env = {repr(env)}')
	# logger("* * * * return evaluate(c.abs.expr, env)")

	return evaluate(c.abs.expr, env)

def eval_call(e, store):
	assert type(e) is Call_expr
	assert type(store) is dict

	c = evaluate(e.func, store)

	if type(c) is not Closure:
		raise Exception("cannot apply a non-closure to an argument")

	# Evaluate arguments
	args = []
	for arg in e.args:
		assert isinstance(arg, Expr)
		args += [evaluate(arg, store)]

	# Build the new environment.
	env = clone(c.env)
	for i in range(len(args)):
		env[c.abs.vars[i]] = args[i]

	return evaluate(c.abs.expr, env)

def eval_index_proj(e, store):
	assert type(e) is Index_proj_expr
	assert type(store) is dict

	return evaluate(e.expr, store)[e.proj]

def eval_access_proj(e, store):
	assert type(e) is Access_proj_expr
	assert type(store) is dict

	x = evaluate(e.expr, store)[e.proj]
	return evaluate(x, store)

def eval_aggregate(e, store):
	assert isinstance(e, Aggregate_multary_expr)
	assert type(store) is dict

	ExprNode = type(e)
	if ExprNode is Tuple_expr:
		return ExprNode([evaluate(expr, store) for expr in e.exprs])
	if ExprNode is Record_expr:
		return ExprNode([evaluate(expr, store) for expr in e.exprs], e.fields)
	assert False
