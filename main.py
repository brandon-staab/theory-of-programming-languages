from test_expr import test_expr
from test_lambda import test_lambda
from test_untyped_lambda import test_untyped_lambda
from test_multi_arg_lambda import test_multi_arg_lambda
from test_aggregate import test_aggregate
from test_generic import test_generic


def main():
	test_generic()
	print("\n\n\n| ==== * ==== * ==== | ==== * ==== * ==== | ==== * ==== * ==== |\n")

	test_expr()
	test_lambda() # needs recursion
	# test_untyped_lambda() # needs currying
	test_multi_arg_lambda()
	# test_aggregate() # tuples need fixed
	# test_generic()


if __name__ == "__main__":
	main()
