from AST import *


def is_type_bool(x):
	assert isinstance(x, LangAtom)

	if isinstance(x, Expr):
		return is_type_bool(get_expr_type(x))
	if isinstance(x, Type):
		return x == bool_type
	assert False

def is_type_int(x):
	assert isinstance(x, LangAtom)

	if isinstance(x, Expr):
		return is_type_int(get_expr_type(x))
	if isinstance(x, Type):
		return x == int_type
	assert False

# def is_type_float(x):
# 	assert isinstance(x, LangAtom)
#
# 	if isinstance(x, Expr):
# 		return is_type_float(get_expr_type(x))
# 	if isinstance(x, Type):
# 		return x == float_type
#	 assert False
#
# def is_type_complex(x):
# 	assert isinstance(x, LangAtom)
#
# 	if isinstance(x, Expr):
# 		return is_type_complex(get_expr_type(x))
# 	if isinstance(x, Type):
# 		return x == complex_type
#	assert False

def is_same_type(t1, t2):
	assert isinstance(t1, Type)
	assert isinstance(t2, Type)

	if type(t1) is not type(t2):
		return False
	if isinstance(t1, Nullary_type):
		return True
	if type(t1) is Arrow_type:
		return t1.ret == t2.ret and t1.parm == t2.parm
	if type(t1) is Function_type:
		return t1.ret == t2.ret and t1.parms == t2.parms
	assert False

def have_same_type(lhs, rhs):
	assert isinstance(lhs, LangAtom)
	assert isinstance(rhs, LangAtom)

	return is_same_type(get_type(lhs), get_type(rhs))

def get_type(x):
	assert isinstance(x, LangAtom)

	if isinstance(x, Expr):
		return get_expr_type(x)
	if isinstance(x, Decl):
		return get_decl_type(x)
	if isinstance(x, Type):
		return x

def get_decl_type(d):
	assert isinstance(d, Decl)

	# TODO: Documentation

	logger(repr(d))
	# d.type = lookup d.id 's type
	# FIXME: look up where?

	assert d.type is not None
	return d.type

def get_expr_type(e):
	assert isinstance(e, Expr)

	if not e.type:
		e.type = compute_expr_type(e)

	assert isinstance(e.type, Type)
	return e.type

def compute_expr_type(e):
	assert isinstance(e, Expr)

	if isinstance(e, Nullary_expr):
		return compute_nullary_expr_type(e)
	if isinstance(e, Unary_expr):
		return compute_unary_expr_type(e)
	if isinstance(e, Binary_expr):
		return compute_binary_expr_type(e)
	if isinstance(e, Ternary_expr):
		return compute_ternary_expr_type(e)
	if isinstance(e, Multiary_expr):
		return compute_multiary_expr_type(e)
	assert False

def compute_nullary_expr_type(e):
	assert isinstance(e, Nullary_expr)

	if type(e) is Id_expr:
		return compute_id_expr_type(e)
	if isinstance(e, Literal_nullary_expr):
		return compute_literal_nullary_expr_type(e)
	assert False

def compute_id_expr_type(e):
	assert type(e) is Id_expr
	assert isinstance(e.ref, Decl)

	#  x : T in Γ
	# ----------- T-Id
	# Γ ⊢ x : T
	e.ref.type = get_decl_type(e.ref)

	assert isinstance(e.ref.type, Type)
	return e.ref.type

def compute_literal_nullary_expr_type(e):
	assert isinstance(e, Literal_nullary_expr)

	if type(e) is Boolean_lit_expr:
		# ------------ T-Bool
		# Γ ⊢ b : Bool
		return bool_type
	if type(e) is Integer_lit_expr:
		# --------------- T-Integer
		# Γ ⊢ n : Integer
		return int_type
	# if type(e) is Float_lit_expr:
	# 	# ------------- T-Float
	# 	# Γ ⊢ f : Float
	# 	return float_type
	# if type(e) is Complex_lit_expr:
	# 	# --------------- T-Complex
	# 	# Γ ⊢ c : Complex
	# 	return complex_type
	assert False

def compute_unary_expr_type(e):
	assert isinstance(e, Unary_expr)

	if type(e) is Generic_expr:
		return compute_generic_expr_type(e)
	if type(e) is Instantiate_expr:
		return compute_instantiate_expr_type(e)
	if type(e) is Variant_expr:
		return compute_variant_expr_type(e)
	if isinstance(e, Projection_unary_expr):
		return compute_projection_unary_expr_type(e)
	if isinstance(e, LambdaAbstraction_unary_expr):
		return compute_lambda_abstraction_unary_expr_type(e)
	if isinstance(e, Arithmetic_unary_expr):
		return compute_arithmetic_unary_expr_type(e)
	# if isinstance(e, Incdec_unary_expr):
	# 	assert False
	if isinstance(e, Logical_unary_expr):
		return compute_logical_unary_expr_type(e)
	assert False

def compute_generic_expr_type(e):
	assert type(e) is Generic_expr

	ts = e.vars
	t = get_type(e.expr)

	return Universal_type(ts, t)


def compute_instantiate_expr_type(e):
	assert type(e) is Instantiate_expr

	t = get_type(e.expr)
	if type(e) is not Universal_type:
		raise Exception(f"invalid instantiation of {repr(e)}")

	if len(e.args) < len(t.parms):
		raise Exception("too few arguments")
	if len(e.args) > len(t.parms):
		raise Exception("too many arguments")

	sub = {}
	for i in range(len(e.args)):
		sub[t.parms[i]] = e.args[i]

	return substitute(t.type, sub)

def compute_variant_expr_type(e):
	assert type(e) is Variant_expr
	assert e.active in e.fields

	t = Variant_type([Field_decl(field, Placeholder_type()) for field in e.fields])
	for field in t.fields:
		if field.id == e.active:
			field.type = get_type(e.expr)
			return t

	assert False

def compute_projection_unary_expr_type(e):
	assert isinstance(e, Projection_unary_expr)

	# TODO: Documentation

	proj_type = get_type(e.expr)[e.proj]

	if type(e) is Index_proj_expr:
		if type(e.expr.type) is not Tuple_type:
			raise Exception("index to non-tuple")
		if e.proj < 0:
			raise Exception(f"{e.proj} index projection less than bounds")
		if e.proj >= len(e.expr.type.types):
			raise Exception(f"{e.proj} index projection greater than bounds")
	elif type(e) is Access_proj_expr:
		if type(e.expr.type) not in (Record_type, Variant_type):
			raise Exception("access to non- record or variant")
		if e.proj not in e.expr.fields:
			raise Exception(f"{e.proj} access projection not found in [{', '.join(map(str, e.expr.fields))}]")
	else:
		assert False

	return proj_type

def compute_lambda_abstraction_unary_expr_type(e):
	assert isinstance(e, LambdaAbstraction_unary_expr)

	if type(e) is Abstraction_expr:
		return compute_abstraction_expr_type(e)
	if type(e) is Lambda_expr:
		return compute_lambda_expr_type(e)
	assert False

def compute_abstraction_expr_type(e):
	assert type(e) is Abstraction_expr

	#   Γ, x:T1 ⊢ e1 : T2
	# ------------------------ T-Abstraction
	# Γ ⊢ λx:T1.e1 : T1 -> T2
	t1 = e.var.type
	t2 = get_expr_type(e.expr)
	return Arrow_type(t1, t2)

def compute_lambda_expr_type(e):
	assert type(e) is Lambda_expr

	# TODO: Documentation

	parm_types = [get_type(item) for item in e.vars]
	ret_type = get_expr_type(e.expr)

	return Function_type(parm_types, ret_type)

def compute_arithmetic_unary_expr_type(e):
	assert isinstance(e, Arithmetic_unary_expr)

	# Γ ⊢ e : Int
	# -------------- T-Arithmetic Unary
	# Γ ⊢ op e: Int
	if is_type_int(e.expr):
		return int_type
	# Float
	# Complex
	raise Exception(f'invalid operand to "{e.get_operator_token()}".')

def compute_logical_unary_expr_type(e):
	assert isinstance(e, Logical_unary_expr)

	# Γ ⊢ e : Bool
	# -------------- T-Logical Unary
	# Γ ⊢ op e: Bool
	if is_type_bool(e.expr):
		return bool_type
	raise Exception(f'invalid operand to "{e.get_operator_token()}".')

def compute_binary_expr_type(e):
	assert isinstance(e, Binary_expr)

	if type(e) is Application_expr:
		return compute_application_expr_type(e)

	# Γ ⊢ e1 : T1   Γ ⊢ e2 : T1
	# ---------------------------
	#    Γ ⊢ e1 op e2 : T2
	if not have_same_type(e.lhs, e.rhs):
		# logger(repr(f'e.rhs = {repr(e.rhs)} \te.lhs = {repr(e.lhs)}'))
		raise Exception(f'invalid operands to "{e.get_operator_token()}".')

	if isinstance(e, Arithmetic_binary_expr):
		return compute_arithmetic_binary_expr_type(e)
	if isinstance(e, Logical_binary_expr):
		return compute_logical_binary_expr_type(e)
	if isinstance(e, Comparison_binary_expr):
		return compute_comparison_binary_expr_type(e)
	assert False

def compute_application_expr_type(e):
	assert type(e) is Application_expr

	# Γ ⊢ e1 : T1 -> T2   Γ ⊢ e2 : T1
	# --------------------------------- T-Application
	#        Γ ⊢ e1 e2 : T2
	t1 = get_expr_type(e.lhs)
	t2 = get_expr_type(e.rhs)

	if type(t1) is not Arrow_type:
		raise Exception(f"application of '{e.rhs}' to the non-abstraction '{e.lhs}'")

	if not is_same_type(t1.parm, t2):
		logger(f"{repr(t1.parm)} is not the same type as {repr(t2)}")
		raise Exception(f"invalid operand to abstraction to '{e.rhs}', expected type {t1}, but got '{e.rhs}:{t2}'")

	return t2

def compute_arithmetic_binary_expr_type(e):
	assert isinstance(e, Arithmetic_binary_expr)
	assert have_same_type(e.lhs, e.rhs)

	# Γ ⊢ e1 : Int   Γ ⊢ e2 : Int
	# --------------------------- T-Arithmetic Binary
	#    Γ ⊢ e1 op e2 : Int
	if is_type_int(e.lhs):
		return int_type
	# Float
	# Complex
	raise Exception(f'invalid operands to "{e.get_operator_token()}".')

def compute_logical_binary_expr_type(e):
	assert isinstance(e, Logical_binary_expr)
	assert have_same_type(e.lhs, e.rhs)

	# Γ ⊢ e1 : Bool   Γ ⊢ e2 : Bool
	# ----------------------------- T-Logical Binary
	#    Γ ⊢ e1 op e2 : Bool
	if is_type_bool(e.lhs):
		return bool_type
	raise Exception(f'invalid operands to "{e.get_operator_token()}".')

def compute_comparison_binary_expr_type(e):
	assert isinstance(e, Comparison_binary_expr)
	assert have_same_type(e.lhs, e.rhs)

	# Γ ⊢ e1 : T   Γ ⊢ e2 : T
	# ----------------------- T-Comparison Binary
	#    Γ ⊢ e1 op e2 : Bool
	return bool_type

def compute_ternary_expr_type(e):
	assert isinstance(e, Ternary_expr)

	if type(e) is Conditional_expr:
		return compute_conditional_expr_type(e)
	assert False

def compute_conditional_expr_type(e):
	assert type(e) is Conditional_expr

	# Γ ⊢ e1 : T   Γ ⊢ e2 : T
	# ----------------------- T-Conditional
	#    Γ ⊢ e1 op e2 : Bool
	if not is_type_bool(e.e1):
		raise Expression(f'conditional first operand must be type bool. got: {get_expr_type(e.e1)}')
	if not have_same_type(e.e2, e.e3):
		raise Exception(f'conditional second and thrid operands must have same type. got: {(get_expr_type(e.e2), get_expr_type(e.e3))}')
	return get_expr_type(e.e2)

def compute_multiary_expr_type(e):
	assert isinstance(e, Multiary_expr)

	if type(e) is Call_expr:
		return compute_call_expr_type(e)
	if isinstance(e, Aggregate_multary_expr):
		return compute_aggregate_multary_expr_type(e)

	assert False

def compute_call_expr_type(e):
	assert type(e) is Call_expr

	# TODO: Documentation

	func_type = get_expr_type(e.func)
	if type(func_type) is not Function_type:
		raise Exception(f'call to non-function')

	for i in range(len(e.args)):
		if not have_same_type(e.args[i], func_type.parms[i]):
			raise Exception(f'invalid operand to lambda {e.args[i]} at position {i}. expected {func_type}')

	return func_type.ret

def compute_aggregate_multary_expr_type(e):
	assert isinstance(e, Aggregate_multary_expr)

	# TODO: Documentation

	if type(e) is Tuple_expr:
		return Tuple_type([get_type(expr) for expr in e.exprs])
	elif type(e) is Record_expr:
		return Record_type([Field_decl(field, get_type(expr)) for field, expr in zip(e.fields, e.exprs)])
	else:
		assert False
