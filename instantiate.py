from lang import *
from substitute import substitute


def instantiate(e, s = {}):
	assert isinstance(e, Expr)
	assert isinstance(s, dict)

	if isinstance(e, Nullary_expr):
		return instantiate_nullary_expr(e, s)
	if isinstance(e, Unary_expr):
		return instantiate_unary_expr(e, s)
	if isinstance(e, Binary_expr):
		return instantiate_binary_expr(e, s)
	if isinstance(e, Ternary_expr):
		return instantiate_ternary_expr(e, s)
	if isinstance(e, Multiary_expr):
		return instantiate_multiary_expr(e, s)

	raise Exception(f"unknown expression: {repr(e)}")

def instantiate_nullary_expr(e, s):
	assert isinstance(e, Nullary_expr)
	assert isinstance(s, dict)

	if isinstance(e, Literal_nullary_expr):
		return e
	if type(e) is Id_expr:
		return Id_expr(e.id)

	assert False

def instantiate_unary_expr(e, s):
	assert isinstance(e, Unary_expr)
	assert isinstance(s, dict)

	if type(e) is Variant_expr:
		assert False
	elif type(e) is Access_proj_expr:
		e = substitute(e.expr)
		return Access_proj_expr(e, e.proj)
	elif type(e) is Lambda_expr:
		vars = [Var_decl(var.id, var.type) for var in e.vars]
		expr = instantiate(e.expr, s)
		return Lambda_expr(vars, expr)
	elif type(e) is Abstraction_expr:
		var = Var_decl(e.var.id, e.var.type)
		expr = instantiate(e.expr)
		return Abstraction_expr(var, expr)
	elif type(e) is Generic_expr:
		return e
	elif type(e) is Instantiate_expr:
		gen = instantiate(e.expr)
		assert type(gen) is Generic_expr
		assert len(gen.vars) == len(e.args)

		sub = {}
		for i in range(len(e.args)):
			sub[gen.vars[i]] = e.args[i]

		return substitute(gen.expr, sub)
	else:
		expr = instantiate(e.expr)
		Node = type(e)

		return Node(expr)

def instantiate_binary_expr(e, s):
	assert isinstance(e, Binary_expr)
	assert isinstance(s, dict)

	e1 = instantiate(e.lhs)
	e2 = instantiate(e.rhs)
	Node = type(e)

	return Node(e1, e2)


def instantiate_ternary_expr(e, s):
	assert isinstance(e, Ternary_expr)
	assert isinstance(s, dict)

	e1 = instantiate(e.e1)
	e2 = instantiate(e.e2)
	e3 = instantiate(e.e3)
	Node = type(e)

	return Node(e1, e2, e3)

def instantiate_multiary_expr(e, s):
	assert isinstance(e, Multiary_expr)
	assert isinstance(s, dict)

	if type(e) is Call_expr:
		func = instantiate(e.func, s)
		es = [instantiate(x, s) for x in e.args]
		return Call_expr(func, es)

	if type(e) is Tuple_expr:
		exprs = substitute(e.exprs, s)
		return TupleExpr(exprs)

	if type(e) is Record_expr:
		# FIXME
		fields = []
		return Record_expr(fields)

	assert False
