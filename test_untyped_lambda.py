from debug import *

def test_untyped_lambda():
	print('\n\n****************\n*    Lambda    *\n****************')

	# λx.x
	i = Abstraction_expr(
			Var_decl('x', None),
			expr('x'))
	print(f'identity: {i}')

	# λa.λb.a
	t = Abstraction_expr(
			Var_decl('a', None),
			Abstraction_expr(Var_decl('b', None), expr('a')))
	print(f'true = {t}')

	# λa.λb.b
	f = Abstraction_expr(
			Var_decl('a', None),
			Abstraction_expr(Var_decl('b', None), expr('b')))
	print(f'false = {f}')

	# λp.λq.((p q) p)
	l_and = \
		Abstraction_expr(
			Var_decl('p', None),
			Abstraction_expr(
				Var_decl('q', None),
				Application_expr(
					Application_expr(
						expr('p'),
						expr('q')),
					expr('p'))))
	print(f'l_and = {l_and}\n')

	# ((λp.λq.((p q) p) λa.λb.a) λa.λb.b) -> λa.λb.b
	lamb = Application_expr(Application_expr(l_and, t), f)
	debug(lamb)

	# (λx.x λx.x) -> λx.x
	debug(Application_expr(i, i))

	# ((λx.x λx.x) λx.x) -> λx.x
	debug(Application_expr(Application_expr(i, i), i))

	# λk.k
	k = Abstraction_expr(Var_decl('k', None), expr('k'))
	# ((λx.x λx.x) λk.k) -> λk.k
	debug(Application_expr(Application_expr(i, i), k))

	# (λx.x 4) -> 4
	debug(Application_expr(
		Application_expr(i, i),
		Integer_lit_expr(4)))

	# λside:int.(side * side)
	square = \
		Abstraction_expr(
			Var_decl('side', int_type),
			Multiplication_expr(
				expr('side'),
				expr('side')))

	# (λside.(side * side) 4)
	call = Application_expr(square, Integer_lit_expr(4))
	debug(call)
