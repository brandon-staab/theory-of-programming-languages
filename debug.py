from lang import *


def debug(e):
	print(f"\n================================================================================================\n")

	resolve(e)
	print(f"Expression: {e}")

	e = instantiate(e)
	resolve(e)
	print(f"Instantiated to: {e}")

	print(f"Height: {height(e)}")
	print(f"Size: {size(e)}")

	# print(f"get_expr_type(e) = {get_expr_type(e)}")
	try:
		print(f"Type: {get_expr_type(e)}")
	except Exception as ex:
		print(f"Type checking exception: {ex}")

	# logger(f"evaluate(e) = {evaluate(e)}")
	big = None
	try:
		big = evaluate(e)
	except Exception as ex:
		print(f"Evaluate exception: {ex}")
	print(f"Value: {big}")

	print(f"-------------------------------- Reduction --------------------------------")
	# logger(f"reduce(e) = {reduce(e)}")
	small = None
	try:
		small = reduce(e)
	except Exception as ex:
		print(f"Reduce exception: {ex}")

	# Check if worked
	try:
		debug_is_same(big, small)
	except Exception as ex:
		print(f"is_same exception: {ex}")


def debug_is_same(big, small):
	if not isinstance(big, (str, bool, int, float, Expr)):
		raise Exception(f"Evaluate Failed, {repr(big)}")
	if not isinstance(small, (str, bool, int, float, Expr)):
		raise Exception(f"Reduction Failed, {repr(small)}")
	if not is_same(big, small):
		Exception(f"Not Same, {repr(big)} != {repr(small)}")
