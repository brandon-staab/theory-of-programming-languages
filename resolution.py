from lang import *


#### Scope ####################################################

class Scope:
	"""Represents a scope."""
	def __init__(self):
		self.decls = {}

	def __str__(self):
		return f'{ {", ".join(map(str, self.decls))} }'

	def __repr__(self):
		return f'<S|{ {", ".join(map(repr, self.decls))} }|{address(self)}>'

	def lookup(self, sym):
		assert type(sym) is str

		if not is_valid_id_name(sym):
			raise Exception(f'Could not lookup {sym} because it is invalid.')
		# logger(f'Looked up {sym} in {self}.')
		return self.decls.get(sym, None)

	def declare(self, sym, decl):
		assert type(sym) is str
		assert isinstance(decl, Decl)
		assert sym == decl.id

		logger(f"decl:Var_decl = {repr(decl)}")
		# logger(f'Declare {repr(decl)} in {repr(self)}.')

		if not is_valid_id_name(sym):
			raise Exception(f'Could not declare {sym} because it is invalid.')
		if sym in self.decls:
			raise Exception(f'Redeclaration of {sym}.')
		self.decls[sym] = decl

class Environment:
	"""Represents a stack of scopes."""
	def __init__(self):
		self.stacks = []

	def __str__(self):
		return f'[{", ".join(map(str, self.stacks))}]'

	def __repr__(self):
		return f'<SS|[{", ".join(map(repr, self.stacks))}]|{address(self)}>'

	def depth(self):
		return len(self.stacks)

	def has_scope(self):
		return self.depth() != 0

	def require_scope(self):
		if not self.has_scope():
			raise Exception("Environment does not have a scope")

	def lookup(self, sym):
		assert type(sym) is str

		# logger(f'Looked up {sym} in {repr(self)}.')
		self.require_scope()

		# Look sym from nearest to furthest scope
		for scope in reversed(self.stacks):
			decl = scope.lookup(sym)
			if decl:
				return decl
		# sym could not be found
		return None

	def declare(self, sym, decl):
		assert type(sym) is str
		assert isinstance(decl, Decl)
		assert sym == decl.id

		# logger(f'Declare {repr(decl)} in {repr(self)}.')

		self.require_scope()
		self.top().declare(sym, decl)

	def enter(self):
		logger(f'Scope entered, size: {self.depth() + 1}')
		self.stacks.append(Scope())

	def leave(self):
		logger(f'Scope exited, size: {self.depth()}')
		self.require_scope()
		self.stacks.pop()

	def top(self):
		return self.stacks[-1]

#### Name Resolution ##########################################

def resolve(x, env = Environment()):
	assert isinstance(x, LangAtom)
	assert type(env) is Environment

	if isinstance(x, Decl):
		return resolve_decl(x, env)
	if isinstance(x, Expr):
		return resolve_expr(x, env)
	if isinstance(x, Type):
		return resolve_type(x, env)

	logger(f"Assert: {repr(x)}")
	assert False

def resolve_decls(ds, env):
	for d in ds:
		assert isinstance(d, Decl)
		resolve_decl(d, env)
	return ds

def resolve_exprs(es, env):
	for e in es:
		assert isinstance(e, Expr)
		resolve_expr(e, env)
	return es

def resolve_types(ts, env):
	for t in ts:
		assert isinstance(t, Type)
		resolve_type(t, env)
	return ts

def resolve_decl(d, env):
	assert isinstance(d, Decl)

	if type(d) is Var_decl:
		resolve_type(d.type, env)
	else:
		assert False

	return d

def resolve_expr(e, env):
	"""
	Resolve references to declared variables. This requires an
	Environment. A scope is a mappings from names to their
	declarations.

	Returns the modified tree.
	"""
	assert isinstance(e, Expr)
	assert type(env) is Environment

	if type(e) is Id_expr:
		# Perform name resolution look.
		decl = env.lookup(e.id)
		if not decl:
			raise Exception(f'{e.id} could not be found in the env {env}')

		# Bind the expression to its declaration.
		e.ref = decl
		# logger(f"e.ref = {repr(e.ref)}")
		return e

	if type(e) is Generic_expr:
		env.enter()

		for var in e.vars:
			env.declare(var.id, var)
		resolve_expr(e.expr, env)

		env.leave()
		return e

	if type(e) is Instantiate_expr:
		resolve_expr(e.expr, env)
		resolve_types(e.args, env)
		return e

	if isinstance(e, LambdaAbstraction_unary_expr):
		env.enter()

		if type(e) is Abstraction_expr:
			# λx.e -> (λx.e) x
			# logger(f'env.declare({repr(e.var.id)}, {repr(e.var)})')
			env.declare(e.var.id, e.var)
			resolve_expr(e.expr, env)
		elif type(e) is Lambda_expr:
			# λ(x, y, z).e -> (λ(x, y, z).e) (x, y, z)
			resolve_decls(e.vars, env)
			for var in e.vars:
				env.declare(var.id, var)
			resolve_expr(e.expr, env)
		else:
			assert False

		env.leave()
		return e

	if type(e) is Call_expr:
		# Recursively resolve_expr in each subexpression.
		resolve_expr(e.func, env)
		for arg in e.args:
			resolve_expr(arg, env)
		return e

	if isinstance(e, Nullary_expr):
		return e

	if isinstance(e, Unary_expr):
		resolve_expr(e.expr, env)
		return e

	if isinstance(e, Binary_expr):
		resolve_expr(e.lhs, env)
		resolve_expr(e.rhs, env)
		return e

	if isinstance(e, Ternary_expr):
		resolve_expr(e.e1, env)
		resolve_expr(e.e2, env)
		resolve_expr(e.e3, env)
		return e

	if isinstance(e, Aggregate_multary_expr):
		resolve_exprs(e.exprs, env)
		return e

	logger(f'Assert: {repr(e)}')
	assert False

def resolve_type(t, env):
	assert isinstance(t, Type)

	if isinstance(t, Primitive_nullary_type):
		return t
	if type(t) is Function_type:
		resolve_types(t.parms, env)
		resolve_type(t.ret, env)
		return t
	# if type(t) is Reference_type:
	# 	resolve_type(t.ret, env)
	# 	return t
	if type(t) is Tuple_type:
		resolve_types(t.elms, env)
		return t
	if type(t) is Record_type:
		for f in t.fields:
			resolve(f.type, env)
		return t
	if type(t) is Variant_type:
		for f in t.fields:
			resolve(f.type, env)
		return t

	if type(t) is Id_type:
		d = env.lookup(t.id)
		if not d:
			logger(env)
			raise Exception("name lookup error")
		if type(d) is not Type_decl:
			logger(env)
			raise Exception(f"'{repr(d)}' does not declare a type")

		# Bind the expression to its declaration
		t.ref = d
		return t

	if type(t) is Universal_type:
	# if isinstance(t, (Universal_type, Existential_type)):
		env.enter()

		for p in t.parms:
			env.declare(p.id, p)
		resolve(t.type, env)

		env.leave()
		return t

	logger(f'Assert: {repr(t)}')
	assert False
