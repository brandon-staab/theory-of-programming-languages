#### Helper Functions #########################################

from utility import logger, address, is_valid_id_name

def decl(x):
	if isinstance(x, Decl):
		return x
	if type(x) is tuple:
		return Var_decl(x[0], x[1])
	assert False

def expr(x):
	if isinstance(x, Expr):
		return x
	if type(x) is str:
		return Id_expr(x)
	if type(x) is bool:
		return Boolean_lit_expr(x)
	if type(x) is int:
		return Integer_lit_expr(x)
	if type(x) is float:
		return Float_lit_expr(x)
	# if type(x) is long:
	# 	return Long_lit_expr(x)
	# if type(x) is complex:
	# 	return Complex_lit_expr(x)
	logger(f"Assert: {repr(x)}")
	assert False

def type_expr(x):
	if isinstance(x, Type):
		return x
	if type(x) is str:
		return Id_type(x)
	if x is bool:
		return bool_type
	if x is int:
		return int_type
	if x is float:
		return float_type
	# if x is long:
	# 	return long_type
	# if x is complex:
	# 	return complex_type
	logger(f"Assert: {repr(x)}")
	assert False

def type_decl(x):
	if type(x) is str:
		return Type_decl(x)
	if type(x) is Type_decl:
		return x
	assert False

###############################################################

class LangAtom:
	"""All parts of the AST inherit from this."""
	def __str__(self):
		return "atom"

	def __repr__(self):
		return f"<{self}|{address(self)}>"

#########################
#    Declaration AST    #
#########################

class Decl(LangAtom):
	"""
	d ::= var      -- variable declaraction
	    | field    -- field declaration
	    | fun      -- function declaraction
	    | pgm      -- program declaraction
	"""
	def __init__(self, id):
		if not is_valid_id_name(id):
			raise Exception(f"{id} is not a valid identifier spelling")
		self.id = id

	def __str__(self):
		return f"{self.id}"

#### Nullary ##################################################
#### Abstract #################

class Nullary_decl(Decl):
	def __init__(self, id):
		super().__init__(id)

	def __str__(self):
		return f"{self.id}"

class Typed_nullary_decl(Nullary_decl):
	def __init__(self, id, type):
		super().__init__(id)
		self.type = type_expr(type)

	def __str__(self):
		return f"{self.id}:{self.type}"

#### Concrete #################

class Type_decl(Nullary_decl):
	"""Represents the declaration of a type variable."""
	def __repr__(self):
		return f"<type_decl|{self}|{address(self)}>"

## Typed Nullary Declaration

class Var_decl(Typed_nullary_decl):
	"""Represents the declaration of a variable."""
	def __repr__(self):
		return f"<var_decl|{self}|{address(self)}>"

class Field_decl(Typed_nullary_decl):
	"""A field is a labeled (i.e., named) member of a record or variant, spelled 'x:T'."""
	def __repr__(self):
		return f"<field_decl|{self}|{address(self)}>"

########################
#    Expression AST    #
########################

class Expr(LangAtom):
	"""
	v ::= u                         -- unit
	    | b                         -- boolean
	    | n                         -- integer
	    | f                         -- float

	e ::= v                         -- value literal
	    | x                         -- variable
	    | _                         -- placeholder
	    | {e1 e2 ... en}.N          -- index projection
	    | {e1 e2 ... en}.x          -- access projection
	    | λx.e                      -- abstraction
	    | λ(x1, x2, ..., xn).e      -- lambda
	    | ~e                        -- binary not
	    | -e                        -- minus
	    | /e                        -- reciprocal
	    | not e                     -- logical not
	    | e1; e2                    -- sequence
	    | e1 e2                     -- application
	    | e1 & e2                   -- binary and
	    | e1 | e2                   -- binary or
	    | e1 ^ e2                   -- binary xor
	    | e1 << e2                  -- binary left shift
	    | e1 >> e2                  -- binary right shift
	    | e1 + e2                   -- addition
	    | e1 - e2                   -- subtraction
	    | e1 * e2                   -- multiplication
	    | e1 / e2                   -- quotient
	    | e1 % e2                   -- modulo
	    | e1 and e2                 -- logical and
	    | e1 or e2                  -- logical or
	    | e1 == e2                  -- equality comparison
	    | e1 != e2                  -- not equal comparison
	    | e1 < e2                   -- less than comparison
	    | e1 > e2                   -- greater than comparison
	    | e1 <= e2                  -- less than or equal comparison
	    | e1 >= e2                  -- greater than or equal comparison
	    | e1 ? e2 : e3              -- conditional
	    | e0(e1, e2, ..., en)       -- call
	    | {e1 e2 ... en}            -- tuple
	    | record {e1 e2 ... en}     -- record
	    | variant {e1 e2 ... en}    -- variant
	"""
	def __init__(self):
		self.type = None

#### Nullary ##################################################
#### Abstract #################

class Nullary_expr(Expr):
	def __init__(self):
		super().__init__()

class Literal_nullary_expr(Nullary_expr):
	def __init__(self, value):
		super().__init__()
		self.value = value
		get_type(self)

	def __str__(self):
		return f"{self.value}"

	def __repr__(self):
		return f"<lit|{self}|type:{self.type}|{address(self)}>"

class Numeric_lit_expr(Literal_nullary_expr):
	def __init__(self, value):
		super().__init__(value)

#### Concrete #################

class Id_expr(Nullary_expr):
	"""Represents identifiers that refer to variables."""
	def __init__(self, id):
		assert is_valid_id_name(id)
		super().__init__()
		self.id = id;
		self.ref = None

	def __str__(self):
		return f"{self.id}"

	def __repr__(self):
		return f"<id:{self}|ref:{self.ref}|type:{self.type}|{address(self)}>"

# class Placeholder_expr(Nullary_expr):
# 	"""Represents a placeholder for an argument to a call."""
# 	def __str__(self):
# 		return "_"
#
# 	def __repr__(self):
# 		return f"<_|{address(self)}>"

## Literal Nullary Expression

# class Unit_lit_expr(Literal_nullary_expr):
# 	"""Represents a unit literal."""
# 	def __init__(self):
# 		pass
#
# 	def __str__(self):
# 		return "Unit"

class Boolean_lit_expr(Literal_nullary_expr):
	"""Represents a boolean literal."""
	def __init__(self, value):
		assert type(value) is bool
		super().__init__(value)

# class String_lit_expr(Literal_nullary_expr):
# 	"""Represents a string literal."""
# 	def __init__(self, value):
# 		assert type(value) is str
# 		super().__init__(value)

### Numeric Literal Nullary Expression

class Integer_lit_expr(Numeric_lit_expr):
	"""Represents an integer literal."""
	def __init__(self, value):
		assert type(value) is int
		super().__init__(value)

class Float_lit_expr(Numeric_lit_expr):
	"""Represents a float literal."""
	def __init__(self, value):
		assert type(value) is float
		super().__init__(value)

# class Long_lit_expr(Numeric_lit_expr):
# 	"""Represents a long number literal."""
# 	def __init__(self, value):
# 		assert type(value) is long
# 		super().__init__(value)
#
# class Complex_lit_expr(Numeric_lit_expr):
# 	"""Represents a complex number literal."""
# 	def __init__(self, value):
# 		assert type(value) is complex
# 		super().__init__(value)

#### Unary ####################################################
#### Abstract #################

class Unary_expr(Expr):
	def __init__(self, e1):
		super().__init__()
		self.expr = expr(e1)

	def __str__(self):
		return f"{self.get_operator_token()}{self.expr}"

	def __repr__(self):
		return f"<{self}|type:{self.type}|{address(self)}>"

class Projection_unary_expr(Unary_expr):
	def __init__(self, e1, proj):
		super().__init__(e1)
		self.proj = proj

class LambdaAbstraction_unary_expr(Unary_expr):
	def __init__(self, e1):
		super().__init__(e1)

class Arithmetic_unary_expr(Unary_expr):
	def __init__(self, e1):
		super().__init__(e1)

# class Incdec_unary_expr(Unary_expr):
# 	def __init__(self, e1):
# 		super().__init__(e1)

class Logical_unary_expr(Unary_expr):
	def __init__(self, e1):
		super().__init__(e1)

#### Concrete #################

class Generic_expr(Unary_expr):
	"""
	Terms of the form 'λω[ti].e'. Here, Each ti is a type variable and
	e is an expression using those types (usually a	lambda expression).
	"""
	def __init__(self, vars, e1):
		super().__init__(e1)
		self.vars = list(map(type_decl, vars))

	def __str__(self):
		parms = ", ".join(map(str, self.vars))
		return f"λω[{parms}].({self.expr})"

	def __repr__(self):
		return f"<gen|{self}|type:{self.type}|{address(self)}>"

class Instantiate_expr(Unary_expr):
	"""
	Terms of the form 'e1 [ti]'. Represents the substitution of types
	into generic expressions to produce concrete expressions.
	"""
	def __init__(self, e1, args):
		super().__init__(e1)
		self.args = list(map(type_expr, args))

	def __str__(self):
		ts = ", ".join(map(str, self.args))
		return f"{self.expr} [{ts}]"

	def __repr__(self):
		return f"<inst|{self}|type:{self.type}|{address(self)}>"

# class Aggregate_multary_expr(Multiary_expr):
# 	def __init__(self, exprs):
# 		super().__init__()
# 		self.exprs = list(map(expr, exprs))
#
# class Record_expr(Aggregate_multary_expr):
# 	"""Represents a record of the form 'record {f1 f2 ... fn}'."""
# 	def __init__(self, exprs, fields):
# 		assert type(fields) is list
# 		for field in fields:
# 			assert type(field) is str
# 		assert len(exprs) == len(fields)
#
# 		super().__init__(exprs)
# 		self.fields = fields
#
# 	def __str__(self):
# 		assert type(self.fields) is list
# 		assert type(self.exprs) is list
# 		assert len(self.fields) == len(self.exprs)
#
# 		x = lambda pair: f"{pair[0]} = {pair[1]}"
# 		return "{" + ", ".join(map(x, zip(self.fields, self.exprs))) + "}"
#
# 	def __repr__(self):
# 		return f"<record|{self}|type:{self.type}|{address(self)}>"
#
# 	def __getitem__(self, key):
# 		assert type(key) is str
# 		for i in range(len(self.fields)):
# 			if self.fields[i] == key:
# 				return self.exprs[i]
# 		return None

class Variant_expr(Unary_expr):
	"""Represents a variant of the form 'variant {f1 f2 ... fn}'."""
	def __init__(self, expr, fields, active):
		assert type(active) is str
		assert active in fields
		assert type(fields) is list
		for field in fields:
			assert type(field) is str

		super().__init__(expr)
		self.fields = fields
		self.active = active

	def __str__(self):
		return f"<{self.active} = {self.expr}> as {get_type(self)}"

	def __repr__(self):
		return f"<variant|{self}|type:{self.type}|{address(self)}>"

	def __getitem__(self, key):
		assert type(key) is str
		assert key in self.fields
		assert key == self.active

		return self.expr

## Projection Unary Expression

# TODO: index could be an int expr
class Index_proj_expr(Projection_unary_expr):
	"""Represents an indexed projection."""
	def __init__(self, e1, idx):
		assert type(idx) is int
		super().__init__(e1, idx)

	def __str__(self):
		return f"{self.expr}[{self.proj}]"

	def __repr__(self):
		return f"<idx_proj|{self}|type:{self.type}|{address(self)}>"

# TODO: id could be a str expr
class Access_proj_expr(Projection_unary_expr):
	"""Represents an access projection."""
	def __init__(self, e1, id):
		assert is_valid_id_name(id)
		super().__init__(e1, id)

	def __str__(self):
		return f"{self.expr}.{self.proj}"

	def __repr__(self):
		return f"<access_proj|{self}|type:{self.type}|{address(self)}>"

## Lambda Abstraction Unary Expression

class Abstraction_expr(LambdaAbstraction_unary_expr):
	"""Represents a lambda abstractions of the form 'λx.e1'."""
	def __init__(self, var, expr):
		super().__init__(expr)
		self.var = decl(var)

	def __str__(self):
		return f"λ{self.var}.{self.expr}"

	def __repr__(self):
		return f"<abs|{self}|type:{self.type}|{address(self)}>"

class Lambda_expr(LambdaAbstraction_unary_expr):
	"""
	Represents multi-argument lambda abstractions.
	Note: 'λ(x, y, z).e' is syntactic sugar for 'λx.λy.λz.e'.
	"""
	def __init__(self, vars, expr):
		super().__init__(expr)
		self.vars = list(map(decl, vars))

	def __str__(self):
		parms = ",".join(map(str, self.vars))
		return f"λ({parms}).{self.expr}"

	def __repr__(self):
		return f"<lambda|{self}|type:{self.type}|{address(self)}>"

## Arithmetic Unary Expression

class B_NOT_expr(Arithmetic_unary_expr):
	"""Represents the bitwise not operator."""
	def __init__(self, e1):
		super().__init__(e1)

	def get_operator_token(self):
		return "~"

	def get_operation():
		return lambda x: ~x

class Minus_expr(Arithmetic_unary_expr):
	"""Represents the minus operator."""
	def __init__(self, e1):
		super().__init__(e1)

	def get_operator_token(self):
		return "-"

	def get_operation():
		return lambda x: -x

class Reciprocal_expr(Arithmetic_unary_expr):
	"""Represents the reciprocal operator."""
	def __init__(self, e1):
		super().__init__(e1)

	def get_operator_token(self):
		return "/"

	def get_operation():
		return lambda x: 1 / x

# ## Increment/Decrement Unary Expression
#
# class pre_Increment_expr(Incdec_unary_expr):
# 	def __init__(self, e1):
# 		super().__init__(e1)
#
# 	def get_operator_token(self):
# 		return "++"
#
# class pre_Decrement_expr(Incdec_unary_expr):
# 	def __init__(self, e1):
# 		super().__init__(e1)
#
# 	def get_operator_token(self):
# 		return "--"
#
# class post_Increment_expr(Incdec_unary_expr):
# 	def __init__(self, e1):
# 		super().__init__(e1)
#
# 	def __str__(self):
# 		return f"{self.expr}++"
#
# 	def get_operator_token(self):
# 		return "++"
#
# class post_Decrement_expr(Incdec_unary_expr):
# 	def __init__(self, e1):
# 		super().__init__(e1)
#
# 	def __str__(self):
# 		return f"{self.expr}--"
#
# 	def get_operator_token(self):
# 		return "--"

## Logical Unary Expression

class L_NOT_expr(Logical_unary_expr):
	"""Represents the logical not operator."""
	def __init__(self, e1):
		super().__init__(e1)

	def get_operator_token(self):
		return "!"

	def get_operation():
		return lambda x: not x

#### Binary ###################################################
#### Abstract #################

class Binary_expr(Expr):
	def __init__(self, lhs, rhs):
		super().__init__()
		self.lhs = expr(lhs)
		self.rhs = expr(rhs)

	def __str__(self):
		return f"({self.lhs} {self.get_operator_token()} {self.rhs})"

	def __repr__(self):
		return f"<{self}|type:{self.type}|{address(self)}>"

class Arithmetic_binary_expr(Binary_expr):
	def __init__(self, lhs, rhs):
		super().__init__(lhs, rhs)

class Logical_binary_expr(Binary_expr):
	def __init__(self, lhs, rhs):
		super().__init__(lhs, rhs)

class Comparison_binary_expr(Binary_expr):
	def __init__(self, lhs, rhs):
		super().__init__(lhs, rhs)

#### Concrete #################

# class Sequence_expr(Binary_expr):
# 	"""
# 	Represents sequence expression of the form 'e1; e2'.
#
# 	Note: This is syntactic sugar for '(λ_.e2) e1'.
# 	The parameter name is essentially ignored.
# 	"""
# 	def __init__(self, lhs, rhs):
# 		super().__init__(lhs, rhs)
#
# 	def __str__(self):
# 		return f"({self.lhs}; {self.rhs})"
#
# 	def __repr__(self):
# 		return f"<sqe|{self}|type:{self.type}|{address(self)}>"


class Application_expr(Binary_expr):
	"""Represents applications of the form 'e1 e2'."""
	def __init__(self, lhs, rhs):
		super().__init__(lhs, rhs)

	def __str__(self):
		return f"({self.lhs} {self.rhs})"

	def __repr__(self):
		return f"<app|{self}|type:{self.type}|{address(self)}>"

## Arithmetic Binary Expression

class B_AND_expr(Arithmetic_binary_expr):
	"""Represents the bitwise and operator."""
	def __init__(self, lhs, rhs):
		super().__init__(lhs, rhs)

	def get_operator_token(self):
		return "&"

	def get_operation():
		return lambda x, y: x & y

class B_OR_expr(Arithmetic_binary_expr):
	"""Represents the bitwise or operator."""
	def __init__(self, lhs, rhs):
		super().__init__(lhs, rhs)

	def get_operator_token(self):
		return "|"

	def get_operation():
		return lambda x, y: x | y

class B_XOR_expr(Arithmetic_binary_expr):
	"""Represents the bitwise exclusive or operator."""
	def __init__(self, lhs, rhs):
		super().__init__(lhs, rhs)

	def get_operator_token(self):
		return "^"

	def get_operation():
		return lambda x, y: x ^ y

class B_LSHIFT_expr(Arithmetic_binary_expr):
	"""Represents the bitwise left shift operator."""
	def __init__(self, lhs, rhs):
		super().__init__(lhs, rhs)

	def get_operator_token(self):
		return "<<"

	def get_operation():
		return lambda x, y: x << y

class B_RSHIFT_expr(Arithmetic_binary_expr):
	"""Represents the bitwise right shift operator."""
	def __init__(self, lhs, rhs):
		super().__init__(lhs, rhs)

	def get_operator_token(self):
		return ">>"

	def get_operation():
		return lambda x, y: x >> y

# class B_LROTATE_expr(Arithmetic_binary_expr):
#	"""Represents the bitwise left rotate operator."""
# 	def __init__(self, lhs, rhs):
# 		super().__init__(lhs, rhs)
#
# 	def get_operator_token(self):
# 		return "<<="
#
# class B_RROTATE_expr(Arithmetic_binary_expr):
#	"""Represents the bitwise right rotate operator."""
# 	def __init__(self, lhs, rhs):
# 		super().__init__(lhs, rhs)
#
# 	def get_operator_token(self):
# 		return "=>>"

class Addition_expr(Arithmetic_binary_expr):
	"""Represents the addition operator."""
	def __init__(self, lhs, rhs):
		super().__init__(lhs, rhs)

	def get_operator_token(self):
		return "+"

	def get_operation():
		return lambda x, y: x + y

class Subtraction_expr(Arithmetic_binary_expr):
	"""Represents the subtraction operator."""
	def __init__(self, lhs, rhs):
		super().__init__(lhs, rhs)

	def get_operator_token(self):
		return "-"

	def get_operation():
		return lambda x, y: x - y

class Multiplication_expr(Arithmetic_binary_expr):
	"""Represents the multiplication operator."""
	def __init__(self, lhs, rhs):
		super().__init__(lhs, rhs)

	def get_operator_token(self):
		return "*"

	def get_operation():
		return lambda x, y: x * y

class Quotient_expr(Arithmetic_binary_expr):
	"""Represents the quotient operator."""
	def __init__(self, lhs, rhs):
		super().__init__(lhs, rhs)

	def get_operator_token(self):
		return "/"

	def get_operation():
		return lambda x, y: x / y

class Modulo_expr(Arithmetic_binary_expr):
	"""Represents the modulo operator."""
	def __init__(self, lhs, rhs):
		super().__init__(lhs, rhs)

	def get_operator_token(self):
		return "%"

	def get_operation():
		return lambda x, y: x % y

## Logical Binary Expression

class L_AND_expr(Logical_binary_expr):
	"""Represents the logical and operator."""
	def __init__(self, lhs, rhs):
		super().__init__(lhs, rhs)

	def get_operator_token(self):
		return "&&"

	def get_operation():
		return lambda x, y: x and y

class L_OR_expr(Logical_binary_expr):
	"""Represents the logical or operator."""
	def __init__(self, lhs, rhs):
		super().__init__(lhs, rhs)

	def get_operator_token(self):
		return "||"

	def get_operation():
		return lambda x, y: x or y

## Comparison Binary Expression

class EQ_expr(Comparison_binary_expr):
	"""Represents the equality operator."""
	def __init__(self, lhs, rhs):
		super().__init__(lhs, rhs)

	def get_operator_token(self):
		return "=="

	def get_operation():
		return lambda x, y: x == y

class NEQ_expr(Comparison_binary_expr):
	"""Represents the not equal operator."""
	def __init__(self, lhs, rhs):
		super().__init__(lhs, rhs)

	def get_operator_token(self):
		return "!="

	def get_operation():
		return lambda x, y: x != y

class LT_expr(Comparison_binary_expr):
	"""Represents the less than operator."""
	def __init__(self, lhs, rhs):
		super().__init__(lhs, rhs)

	def get_operator_token(self):
		return "<"

	def get_operation():
		return lambda x, y: x < y

class GT_expr(Comparison_binary_expr):
	"""Represents the greater than operator."""
	def __init__(self, lhs, rhs):
		super().__init__(lhs, rhs)

	def get_operator_token(self):
		return ">"

	def get_operation():
		return lambda x, y: x > y

class LTEQ_expr(Comparison_binary_expr):
	"""Represents the less than or equal operator."""
	def __init__(self, lhs, rhs):
		super().__init__(lhs, rhs)

	def get_operator_token(self):
		return "<="

	def get_operation():
		return lambda x, y: x <= y

class GTEQ_expr(Comparison_binary_expr):
	"""Represents the greater than or equal operator."""
	def __init__(self, lhs, rhs):
		super().__init__(lhs, rhs)

	def get_operator_token(self):
		return ">="

	def get_operation():
		return lambda x, y: x >= y

#### Ternary ##################################################
#### Abstract #################

class Ternary_expr(Expr):
	def __init__(self, e1, e2, e3):
		super().__init__()
		self.e1 = expr(e1)
		self.e2 = expr(e2)
		self.e3 = expr(e3)

#### Concrete #################

class Conditional_expr(Ternary_expr):
	"""Represents a conditional of the form 'e1 ? e2 : e3'."""
	def __init__(self, e1, e2, e3):
		super().__init__(e1, e2, e3)

	def __str__(self):
		return f"({self.e1} ? {self.e2} : {self.e3})"

	def __repr__(self):
		return f"<{self}|type:{self.type}|{address(self)}>"

	def get_operation():
		return lambda condition, true_expr, false_expr: true_expr if condition else false_expr \
		       if type(condition) is bool else exit("condition is not bool")

#### Multiary ###################################################
#### Abstract #################

class Multiary_expr(Expr):
	def __init__(self):
		super().__init__()

class Aggregate_multary_expr(Multiary_expr):
	def __init__(self, exprs):
		super().__init__()
		self.exprs = list(map(expr, exprs))

#### Concrete #################

class Call_expr(Multiary_expr):
	"""Represents calls of multi-argument lambda abstractions."""
	def __init__(self, func, args):
		super().__init__()
		self.func = expr(func)
		self.args = list(map(expr, args))

	def __str__(self):
		args = ", ".join(map(str, self.args))
		return f"{self.func} ({args})"

	def __repr__(self):
		return f"<call|{self}|type:{self.type}|{address(self)}>"

## Aggregate Multary Expr

class Tuple_expr(Aggregate_multary_expr):
	"""Represents a tuple of the form '{e1 e2 ... en}'."""
	def __str__(self):
		return "{" + ", ".join(map(str, self.exprs)) + "}"

	def __repr__(self):
		return f"<tuple|{self}|type:{self.type}|{address(self)}>"

	def __getitem__(self, key):
		assert type(key) is int
		return self.exprs[key]

class Record_expr(Aggregate_multary_expr):
	"""Represents a record of the form 'record {f1 f2 ... fn}'."""
	def __init__(self, exprs, fields):
		assert type(fields) is list
		for field in fields:
			assert type(field) is str
		assert len(exprs) == len(fields)

		super().__init__(exprs)
		self.fields = fields

	def __str__(self):
		assert type(self.fields) is list
		assert type(self.exprs) is list
		assert len(self.fields) == len(self.exprs)

		x = lambda pair: f"{pair[0]} = {pair[1]}"
		return "{" + ", ".join(map(x, zip(self.fields, self.exprs))) + "}"

	def __repr__(self):
		return f"<record|{self}|type:{self.type}|{address(self)}>"

	def __getitem__(self, key):
		assert type(key) is str
		for i in range(len(self.fields)):
			if self.fields[i] == key:
				return self.exprs[i]
		return None

##################
#    Type AST    #
##################

class Type(LangAtom):
	"""
	T ::= Unit                         -- unit type
	    | _                            -- placeholder
	    | Bool                         -- bool type
	    | Int                          -- integer type
	    | T1 -> T2                     -- arrow type
	    | (T1, T2, ..., Tn) -> T0      -- function type
	    | {T1, T2, ..., Tn}            -- tuple type
	    | record {f1, f2, ..., fn}     -- record type
	    | variant {f1, f2, ..., fn}    -- variant type
	"""
	pass

#### Nullary ##################################################
#### Abstract #################

class Nullary_type(Type):
	def __repr__(self):
		return f"<type|{self}|{address(self)}>"

class Primitive_nullary_type(Nullary_type):
	pass

#### Concrete #################

class Unit_type(Nullary_type):
	def __str__(self):
		return "Unit"

class Placeholder_type(Nullary_type):
	def __str__(self):
		return "_"

class Id_type(Nullary_type):
	def __init__(self, x):
		assert isinstance(x, (str, Type_decl))

		if type(x) is str:
			assert is_valid_id_name(x)
			self.id = x
			self.ref = None
		elif type(x) is Type_decl:
			self.id = x.id
			self.ref = x

	def __str__(self):
		return f"{self.id}"

	def __repr__(self):
		return f"<id_t|{self}|{address(self)}>"

class Dependent_type(Nullary_type):
	"""Represents the dependent part of a type-dependent expression."""
	def __str__(self):
		return "τ"

## Primitive Nullary Type

class Bool_type(Primitive_nullary_type):
	def __str__(self):
		return "Bool"

class Int_type(Primitive_nullary_type):
	def __str__(self):
		return "Int"

# class Float_type(Primitive_nullary_type):
# 	def __str__(self):
# 		return "Float"
#
# class Complex_type(Primitive_nullary_type):
# 	def __str__(self):
# 		return "Complex"

#### Binary ###################################################
#### Abstract #################

class Binary_type(Type):
	pass

#### Concrete #################

class Arrow_type(Binary_type):
	"""
	Represents types of the form 'T1 -> T2'
	"""
	def __init__(self, t1, t2):
		self.parm = t1
		self.ret = t2

	def __str__(self):
		return f"{self.parm} -> {self.ret}"

	def __repr__(self):
		return f"<arrow_t|{self}|{address(self)}>"

#### Multiary ###################################################
#### Abstract #################

class Multiary_type(Type):
	pass

class Aggregate_multary_type(Multiary_type):
	pass

#### Concrete #################

class Function_type(Multiary_type):
	"""Represents types of the form '(T1, T2, ..., Tn) -> T0'."""
	def __init__(self, parms, ret):
		self.parms = list(map(type_expr, parms))
		self.ret = type_expr(ret)

	def __str__(self):
		parms = ", ".join(map(str, self.parms))
		return f"({parms}) -> {self.ret})"

	def __repr__(self):
		return f"<func_t|{self}|{address(self)}>"

class Universal_type(Multiary_type):
	"""
	Types of the form '∀[Xi].T'.
	This is the type of type abstractions (i.e. Generics).
	"""
	# We treat them here as declarations because we can conceivably
	# create type aliases. For example:
	#
	#   Comp = forall [T].(T, T)->Bool;
	#
	# To resolve the uses of T in the generic function type, we'd
	# to have previously declared them.
	#
	# Note that a type abstraction ALSO declares type variables:
	#
	#   min = lambda T.(a:T, b:T).a < b;
	#
	# When we check the type of the (outer) lambda, we simply reuse
	# the same parameters for the computed universal type.
	def __init__(self, ts, t):
		self.parms = [type_decl(t) for t in ts]
		self.type = type_expr(t)

	def __str__(self):
		ts = ", ".join([str(p) for p in self.parms])
		return f"∀[{ts}].{str(self.type)}"

	def __repr__(self):
		return f"<universal_t|{self}|{address(self)}>"

# class Existential_type(Type):
# 	"""
# 	Types of the form '∃[Ti].T0'
# 	https://wiki.haskell.org/Existential_type
# 	https://en.wikipedia.org/wiki/Skolem_normal_form
# 	"""
# 	def __init__(self, vs, t):
# 		self.parms = list(map(type_decl, vs))
# 		self.type = type_expr(t)
#
# 	def __str__(self):
# 		ps = ", ".join(map(str, self.parms))
# 		return f"∃[{ps}].{str(self.type)}"

## Aggregate Multary Type

class Tuple_type(Aggregate_multary_type):
	"""The type '{T1, T2, ..., Tn}' whose values are tuples."""
	def __init__(self, types):
		for type in types:
			assert isinstance(type, Type)

		self.types = types

	def __str__(self):
		return "{" + ", ".join(map(str, self.types)) + "}"

	def __repr__(self):
		return f"<tuple_t|{self}|{address(self)}>"

	def __getitem__(self, key):
		assert type(key) is int
		return self.types[key]

class Record_type(Aggregate_multary_type):
	"""The type 'record {f1, f2, ..., fn}'."""
	def __init__(self, fields):
		type(fields) is list
		for field in fields:
			assert type(field) is Field_decl

		self.fields = fields

	def __str__(self):
		return "record {" + ", ".join(map(str, self.fields)) + "}"

	def __repr__(self):
		return f"<record_t|{self}|{address(self)}>"

	def __getitem__(self, key):
		assert type(key) is str
		for field in self.fields:
			if field.id == key:
				return field.type
		return None

class Variant_type(Aggregate_multary_type):
	"""The type 'variant {f1, f2, ..., fn}'."""
	def __init__(self, fields):
		type(fields) is set
		for field in fields:
			assert type(field) is Field_decl

		self.fields = fields

	def __str__(self):
		return "variant {" + ", ".join(map(str, self.fields)) + "}"

	def __repr__(self):
		return f"<variant_t|{self}|{address(self)}>"

	def __getitem__(self, key):
		assert type(key) is str
		for field in self.fields:
			if field.id == key:
				return field.type
		return None

#### Type Instances ###########################################

bool_type = Bool_type()
int_type = Int_type()
# float_type = Float_type()
# complex_type = Complex_type()


from type_checking import get_type, have_same_type, is_type_bool, is_type_int
