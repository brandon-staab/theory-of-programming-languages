from debug import *

def test_multi_arg_lambda():
	print('\n\n**************************\n*    Multi Arg Lambda    *\n**************************')

	create_impl = lambda: \
		Lambda_expr(
			[Var_decl("p", bool_type), Var_decl("q", bool_type)],
			L_OR_expr(L_NOT_expr("p"), "q"))

	logger(f"test {repr(bool_type)}")

	table = [
		Call_expr(create_impl(), [True, True]),
		Call_expr(create_impl(), [True, False]),
		Call_expr(create_impl(), [False, True]),
		Call_expr(create_impl(), [False, False])
	]

	for e in table:
		debug(e)
