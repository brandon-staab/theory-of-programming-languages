from inspect import currentframe
import sys


def logger(s = ""):
	if type(s) is not str:
		s = repr(s)

	if "debug" in sys.argv:
		caller = currentframe().f_back
		num = caller.f_lineno
		filename = caller.f_code.co_filename.rsplit('/',1)[1]
		print(f"{filename : <20} | line {num : <4} | {s}")

def address(obj):
	if obj is None:
		return "nullptr"
	return f"{hex(id(obj))[8:]}"

def is_valid_id_name(id):
	assert type(id) is str

	if not (id[0].isalpha() or id[0] == '_'):
		return False
	for c in id:
		if not (c.isalnum() or c == '_'):
			return False
	return True
