from debug import *


def test_expr():
	test_expr_bool()
	test_expr_int()
	test_expr_conditional()

def test_expr_bool():
	print('\n\n*****************\n*    Boolean    *\n*****************')

	e = L_AND_expr(
				expr(True),
				L_NOT_expr(expr(False))
			)

	assert is_same(e, e)
	assert not is_same(e, L_AND_expr(expr(True), expr(True)))

	debug(e)

def test_expr_int():
	print('\n\n*****************\n*    Integer    *\n*****************')
	# ((5 + (30 * 15)) < (((30 % 5) << 4) - (42 * 16))) -> False
	e = \
	LT_expr(
		Addition_expr(
				expr(5),
				Multiplication_expr(
					expr(30),
					expr(15))),
		Subtraction_expr(
			B_LSHIFT_expr(
				Modulo_expr(
					expr(30),
					expr(5)),
				expr(4)	),
			Multiplication_expr(
				expr(42),
				expr(16))))
	debug(e)

def test_expr_conditional():
	print('\n\n*********************\n*    Conditional    *\n*********************')
	debug(Conditional_expr(EQ_expr(10, 100), Conditional_expr(GT_expr(5, 2), 75, -75), Conditional_expr(LT_expr(54, 24), 175, -175)))
